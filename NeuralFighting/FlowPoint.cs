﻿using Motor;
using SFML.System;
using SFML.Graphics;
using Motor.Utilities;
using Motor.Physics;

namespace NeuralFighting
{
    public class FlowPoint : PhysicsEntity
    {
        PerlinFlowField flowField;
        CircleCollider cc;
        public Color Color;

        public Collider Collider => cc;

        public FlowPoint(float xpos, float ypos, PerlinFlowField flowField) : base(xpos, ypos)
        {
            Mass = 10;
            this.flowField = flowField;
            cc = new CircleCollider(this, 4);
            PosBonds = new FloatRect(new Vector2f(0, 0), (Vector2f)WindowController.WindowSize());
            Color = Mathf.Lerp(Mathf.Lerp(Color.Red, Color.Blue, xpos / 5000), Mathf.Lerp(Color.Cyan, Color.Green, ypos), (xpos - ypos) / 400);
            Color.A = 50;
        }




        public override void FixedUpdate()
        {
            Vector2f noiseDir = Mathf.RadianToVector(flowField.GetValueAt(Position) * Mathf.Pi * 2);
            AddForce(noiseDir / 3);
            Vector2f center = (Vector2f)WindowController.WindowSize() * .5f;

            /*     if (Mathf.Distance(Position, center) < 200)
                 {
                     AddForce(-(center - Position).Normalized() * 0.5f);
                 }
                 */
            base.FixedUpdate();
        }


        public void Draw()
        {
            Drawer.DrawPrimitiveLine(LastPosistion, Position, Color);
        }
    }

}
