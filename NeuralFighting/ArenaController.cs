﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using SFML.Graphics;
using Motor;
using Motor.UI;

namespace NeuralFighting
{
    class ArenaScene : Scene, IStart
    {
        float _wallThickness = 50;
        LineText ScoreDisplay;
        Color _wallColor = Color.Cyan;
        List<Fighter> fighters;

        public ArenaScene()
        {
        }
        

        public void Setup()
        {

            Wall top = new Wall(0, 0);
            top.SetColor(_wallColor);
            top.Size = new Vector2f(WindowController.GetWidht(), _wallThickness);
            Add(top);

            Wall bot = new Wall(0, WindowController.GetHeight() - _wallThickness);
            bot.Size = new Vector2f(WindowController.GetWidht(), _wallThickness);
            bot.SetColor(_wallColor);
            Add(bot);

            Wall left = new Wall(0, 0);
            left.Size = new Vector2f(_wallThickness, WindowController.GetHeight());
            left.SetColor(_wallColor);
            Add(left);

            Wall right = new Wall(WindowController.GetWidht() - _wallThickness, 0);
            right.Size = new Vector2f(_wallThickness, WindowController.GetHeight());
            right.SetColor(_wallColor);
            Add(right);        
        }

        public void Start()
        {
            Setup();
        }
    }
}
