﻿using Motor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using EloNEAT;
namespace NeuralFighting
{
    public class NetworkVisualizer : UiEntity
    {
        RenderTexture renderTexture;
        Sprite sprite;
        NeuralNetwork nn;


        public NetworkVisualizer(float xpos, float ypos) : base(xpos, ypos)
        {
            Resize(Size);
        }

        public void Resize(Vector2f newSize)
        {
            Size = newSize;
            if (renderTexture != null)
            {
                renderTexture.Dispose();
            }
            renderTexture = new RenderTexture((uint)Size.X, (uint)Size.Y);
        }

        public void SetNetwork(NeuralNetwork nn)
        {
            this.nn = nn;
            RedrawNetwork();
        }

        public void RedrawNetwork()
        {
            renderTexture.Clear(Color.Green);
            Drawer.SetRenderTarget(renderTexture);

           
            Drawer.DrawCenterdRect(Size / 2, Size / 4, Color.Blue);

            Drawer.SetRenderTarget(WindowController.RenderingWindow);
            renderTexture.Display();
            if (sprite != null)
            {
                sprite.Dispose();
            }
            sprite = new Sprite(renderTexture.Texture);
        }


        public override void PreDraw()
        {

        }

        public override void Draw()
        {
            sprite.Position = Position;
            Drawer.Draw(sprite);
        }


    }
}
