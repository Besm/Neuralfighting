﻿using Motor;
using SFML.System;
using SFML.Graphics;
using Motor.Utilities;
using Motor.UI;
using EloNEAT;
namespace NeuralFighting
{
    internal class NeuralFighting : MotorMain
    {
        public NeuralFighting()
        {
            Start();
        }

        public void Start()
        {
            WindowController.SetTitle("Neural Fighting");
            WindowController.SetSize(new Vector2u(1400, 900));
            WindowController.CenterWindow();

            Wall hw = new Wall(0, 0) { Size = new Vector2f(100, 1000) };
            Wall ground = new Wall(0, 0) { Size = new Vector2f(1000, 100) };
            ground.Position = new Vector2f(0, WindowController.GetHeight() - 100);

            Scene sandbox = new Scene();
            sandbox.Add(new HumanFighter(300, 100));
            sandbox.Add(new HumanFighter(1340, 800));
            sandbox.Add(hw);
            sandbox.Add(ground);


            Scene flowScene = new Scene();
            PerlinFlowField p = new PerlinFlowField((Vector2f)WindowController.WindowSize())
            {
                Seed = 4,
                Frequency = .005,
                TimeMultiplier = 10,
                Persistence = 0.1,

                PointsAddedPerUpdate = 0,
                InitialPointsAdded = 1000,
            };

            for(int i = 0; i <3; i++)
            {

            p.AddPointsInCircle(i*100,500,200, 900, new Color((byte)(Mathf.RandomFloat()*255),
                (byte)(Mathf.RandomFloat() * 255), (byte)(Mathf.RandomFloat() * 255), 20));
            }


            flowScene.Add(p);


            SceneManager.Add(sandbox);
            SceneManager.Add(new ArenaScene());
            SceneManager.Add(flowScene);
            SceneManager.GoToScene(0);
            SceneManager.Overlayed.Add(new Terminal(0, 0));

            Genome genome = new Genome();
            genome.Genes = new System.Collections.Generic.List<Gene>();
            genome.Genes.Add(new Gene(0, NodeType.Sensor, 0, 2, 1, true));
            genome.Genes.Add(new Gene(1, NodeType.Sensor, 1, 2, 1, false));
            genome.Genes.Add(new Gene(2, NodeType.Sensor, 2, 2, 1, false));
            genome.Genes.Add(new Gene(3, NodeType.Sensor, 0, 3, 1, true));
            genome.Genes.Add(new Gene(4, NodeType.Sensor, 1, 3, 1, true));
            NeuralNetwork nn = new NeuralNetwork(genome);

            var nv = new NetworkVisualizer(200,200);
           
            nv.Resize(new Vector2f(200, 200));
            nv.SetNetwork(nn);
            sandbox.Add(nv);

        }


        public void Main()
        {
            while (WindowController.IsOpen()) UpdateFrame();
        }
    }
}