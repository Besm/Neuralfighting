﻿using SFML.System;
using SFML.Graphics;
using Motor;
using Motor.Utilities;

namespace NeuralFighting
{
    internal class NeuralFighter : Fighter
    {
        public NeuralFighter(float xpos, float ypos) : base(xpos, ypos)
        {
            SetBodyColor(Color.Cyan);
        }

        public override void FixedUpdate()
        {
            Vector2f force = Mathf.RandomVector2F(-1, 1);
            AddForce(force);
            base.FixedUpdate();
        }
    }

}
