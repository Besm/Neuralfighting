﻿using Motor;
using Motor.Physics;
using Motor.Utilities;
using SFML.Graphics;
using SFML.System;

namespace NeuralFighting
{
    public class Bullet : PhysicsEntity, IDrawable, ICollidable 
    {
        private CircleShape _circleShape;
        CircleCollider _collider;

        public Collider Collider { get => _collider; }

        public Bullet(float xpos, float ypos) : base(xpos, ypos)
        {
            Size = new Vector2f(10, 10);
            Mass = 15;
            Friction = .01f;
            ShapeSetup();
            _circleShape.FillColor = Mathf.RandomColor(255);
            _collider = new CircleCollider(this, Size.X);
        }

        public void Draw()
        {
            Drawer.Draw(_circleShape);
        }

        public void PreDraw()
        {
            _circleShape.Position = Position;
        }


        public void ShapeSetup()
        {
            _circleShape = new CircleShape
            {
                Radius = Size.X,
                FillColor = Globals.BulletColler
            };

            _circleShape.Origin = new Vector2f(_circleShape.Radius, _circleShape.Radius);
        }



        public void OnCollisionEnter(Entity other)
        {
            if (other is Fighter f)
            {
                f.AddForce(Velocity);
                Die();
            }
        }
    }
}