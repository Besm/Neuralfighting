﻿using SFML.Graphics;
using SFML.System;
using System;
using Motor;
using Motor.Utilities;
using Motor.Physics;

namespace NeuralFighting
{
    internal abstract class Fighter : PhysicsEntity, IDrawable, ICollidable
    {
        private CircleShape _circleShape;
        private RectangleShape _canonShape;
        private Color _bodyColor;
        private Color _canonColor;
        private long _updatesSinceLastShot;
        CircleCollider _collider;

        protected CircleShape CircleShape { get => _circleShape; set => _circleShape = value; }

        public Collider Collider => _collider;

        public void SetBodyColor(Color color)
        {
            _bodyColor = color;
            _circleShape.FillColor = color;
            _canonColor = _bodyColor;
            _canonShape.FillColor = _canonColor;
        }
        public Color GetBodyColor()
        {
            return _bodyColor;
        }

        public void ShootBullet()
        {
            if (Math.Abs(_updatesSinceLastShot - FixedTime.UpdatesSinceStart) > 15)
            {
                Bullet b = new Bullet(Position.X, Position.Y);
                b.Position += Rotation.Normalized() * (Size.X * 3);
                b.AddForce(Rotation.Normalized() * 13);
                SceneManager.Active.Add(b);
                _updatesSinceLastShot = FixedTime.UpdatesSinceStart;
            }
        }

        protected Fighter(float xpos, float ypos) : base(xpos, ypos)
        {
            Size = new Vector2f(20, 20);
            _collider = new CircleCollider(this, Size.X);
            DrawSetup();
        }

        public void DrawSetup()
        {
            _circleShape = new CircleShape()
            {
                Radius = (Size.X + Size.Y) / 2,
                Origin = new Vector2f(Size.X, Size.Y),
            };

            _canonShape = new RectangleShape()
            {
                Size = new Vector2f(_circleShape.Radius * 2, _circleShape.Radius),
            };
            _canonShape.Origin = new Vector2f(0, _canonShape.Size.Y / 2);
        }

      

        public virtual void Draw()
        {           
            Drawer.Draw(_circleShape);    
            Drawer.Draw(_canonShape);
        }

        public void PreDraw()
        {
            _circleShape.Position = Position;
            _canonShape.Rotation = Rotation.ToDegrees();
            _canonShape.Position = Position;
        }

        public void OnCollisionEnter(Entity other)
        {
        
        }
    }

}
