﻿using Motor;
using SFML.System;
using System;

namespace NeuralFighting
{
    internal class Program
    {
        [STAThread]
        private static void Main()
        {
            NeuralFighting neuralFighting = new NeuralFighting();
            FixedTime.GameClock = new Clock();
            neuralFighting.Main();
        }
    }
}
