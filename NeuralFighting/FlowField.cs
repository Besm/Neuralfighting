﻿using System;
using System.Collections.Generic;
using Motor;
using Motor.Utilities;
using SFML.Graphics;
using SFML.System;
using SharpNoise.Modules;

namespace NeuralFighting
{
    public class PerlinFlowField : Entity, IDrawable, IFixedUpdate, IStart, IDisposable
    {
        private RenderTexture _renderTexture;
        private Sprite _sprite;
        private float _timeMultiplier = 80;

        public PerlinFlowField(Vector2f Size) : base(0,0)
        {
            PerlinNoise = new Perlin();
            this.Size = Size;
        }

        public Perlin PerlinNoise { get; }
        public Color BackColor { get; } = Color.Black;
        public Color PointColor { get; } = new Color(255, 255, 255, 25);
        public int PointsAddedPerUpdate { get; set; }
        public int InitialPointsAdded { get; set; } = 6;
        public float TimeMultiplier { get => _timeMultiplier; set => _timeMultiplier = value; }


        public int Seed
        {
            get => PerlinNoise.Seed;
            set => PerlinNoise.Seed = value;
        }

        public double Lacunarity
        {
            get => PerlinNoise.Lacunarity;
            set => PerlinNoise.Lacunarity = value;
        }

        public double Frequency
        {
            get => PerlinNoise.Frequency;
            set => PerlinNoise.Frequency = value;
        }

        public double Persistence
        {
            get => PerlinNoise.Persistence;
            set => PerlinNoise.Persistence = value;
        }



        public float GetValueAt(Vector2f pos)
        {
            return (float)PerlinNoise.GetValue(pos.X, pos.Y, FixedTime.SecondsSinceStart * TimeMultiplier);
        }



        public void Draw()
        {
            Drawer.Draw(_sprite);
        }

        public void FixedUpdate()
        {
            for (int i = 0; i < PointsAddedPerUpdate; i++)
            {
                AddRandomFlowPoint();
            }
            Drawer.SetRenderTarget(_renderTexture);
            foreach(var point in GetDirectChildren())
            {
                var p = (FlowPoint)(point);
                p.Draw();
            }
            Drawer.SetRenderTarget(WindowController.GetWindowTarget());
        }

        void AddRandomFlowPoint()
        {
            float maxX = WindowController.WindowSize().X;
            float maxY = WindowController.WindowSize().Y;
            float x = Mathf.RandomFloat(0, maxX);
            float y = Mathf.RandomFloat(0, maxY);
            AddChild(new FlowPoint(x, y, this));
        }

        public void AddPointsInCircle(float x, float y, float radius, float amount, Color color)
        {
            for (int i = 0; i < amount; i++)
            {
                Vector2f offset = Mathf.RandomPointInUnitCircle() * radius;
                var fp = new FlowPoint(x + offset.X, y + offset.Y, this) { Color = color };
                AddChild(fp);
            }
        }

        public void Start()
        {
            _renderTexture =
                new RenderTexture((uint)Size.X, (uint)Size.Y);
            _renderTexture.Clear(BackColor);
            _sprite = new Sprite(_renderTexture.Texture);

            for (int i = 0; i < InitialPointsAdded; i++)
            {
                AddRandomFlowPoint();
            }
        }

        public void Dispose()
        {
            ((IDisposable)_renderTexture).Dispose();
            ((IDisposable)_sprite).Dispose();
        }

        public void PreDraw()
        {
        }
    }
}