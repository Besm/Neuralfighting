﻿using Motor;
using SFML.System;
using SFML.Graphics;
using Motor.Physics;

namespace NeuralFighting
{
    internal class Wall : StaticEntity, IDrawable , IStart, ICollidable
    {
        RectangleCollider _collider;
        RectangleShape rectangleShape;
        
        public Wall(float xpos, float ypos) : base(xpos, ypos)
        {
            Size = new Vector2f(100, 100);
            rectangleShape = new RectangleShape(Size);
        }

        public void SetColor(Color c)
        {
            rectangleShape.FillColor = c; 
        }

        public Collider Collider => _collider;

        public void PreDraw()
        {
            rectangleShape.Position = Position;
        }

        public void Draw()
        {
            rectangleShape.Size = Size;
            Drawer.Draw(rectangleShape);
        }

        public void OnCollisionEnter(Entity other)
        {
        }

        public void Start()
        {
            _collider = new RectangleCollider(this);
        }
    }
}
