﻿using SFML.System;
using SFML.Window;
using SFML.Graphics;
using Motor;

namespace NeuralFighting
{
    internal class HumanFighter : Fighter
    {
        public HumanFighter(int xpos, int ypos) : base(xpos, ypos)
        {
            SetBodyColor(Color.Magenta);
        }


        public override void FixedUpdate()
        {
            Vector2f input = new Vector2f(0, 0);
            if (Input.GetKey(Keyboard.Key.A))
            {
                input.X -= 1;
            }
            if (Input.GetKey(Keyboard.Key.D))
            {
                input.X += 1;
            }
            if (Input.GetKey(Keyboard.Key.S))
            {
                input.Y += 1;
            }
            if (Input.GetKey(Keyboard.Key.W))
            {
                input.Y -= 1;
            }

            if (Input.GetButtonPressed(Mouse.Button.Left))
            {
                ShootBullet();
            }

            AddForce(input);
            Rotation = (Input.RelativeMousePosistion - Position);
            base.FixedUpdate();
        }

    }

}
