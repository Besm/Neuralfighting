﻿using Motor;
using Motor.System;
using SFML.System;

namespace NeuralFighting
{
    public class NeuralFightingTerminalCommands
    {
        [TerminalCommand]
        public static void MoveWindowRight(int llo)
        {
            WindowController.RenderingWindow.Position += new Vector2i(llo,0);
            
        }
    }
}
