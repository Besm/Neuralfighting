﻿using System;
using SFML.Graphics;
using SFML.System;

namespace Motor.Utilities
{


    public static class Mathf
    {
        private static Random _random = new Random();

        public const float Pi = (float)Math.PI;

        public static float Sin(float f)
        {
            return (float)Math.Sin(f);
        }
        public static float Cos(float f)
        {
            return (float)Math.Cos(f);
        }
        public static float Distance(Vector2f a, Vector2f b)
        {
            Vector2f c = a - b;
            return (float)Math.Sqrt(c.X * c.X + c.Y * c.Y);
        }

        public static Vector2f RandomPointInUnitCircle()
        {
            float a = RandomFloat() * 2 * Pi;
            return RadianToVector(a) * RandomFloat();
        }

        public static float Distance(float a, float b)
        {
            return Math.Abs(a - b);
        }


        public static float MinMax(int min, int max, int value)
        {
            return Math.Min(Math.Max(min, value), max);
        }

        public static Vector2f MinMax(Vector2f min, Vector2f max, Vector2f value)
        {
            float x = Math.Min(Math.Max(min.X, value.X), max.X);
            float y = Math.Min(Math.Max(min.Y, value.Y), max.Y);
            return new Vector2f(x, y);
        }

        public static Vector2f Multiply(Vector2f a, Vector2f b)
        {
            return new Vector2f(a.X * b.X, b.Y * b.Y);
        }

        public static float MinMax(float min, float max, float value)
        {
            return Math.Min(Math.Max(min, value), max);
        }

        public static float Sqrt(float f)
        {
            return (float)Math.Sqrt(f);
        }

        public static Vector2f RadianToVector(float f)
        {
            return new Vector2f(Cos(f), Sin(f));
        }

        public static float Length(Vector2f v)
        {
            return Sqrt(v.X * v.X + v.Y * v.Y);
        }


        public static byte Lerp(byte a, byte b, byte c)
        {
            return (byte)(a * (1.0f - c) + b * c);
        }

        public static float Lerp(float a, float b, float c)
        {
            return (float)(a * (1.0f - c) + b * c);
        }

        public static Vector2f Lerp(Vector2f a, Vector2f b, float c)
        {
            return a * (1.0f - c) + b * c;
        }

        public static Color Lerp(Color a, Color b, float c)
        {
            byte cR = (byte)Lerp(a.R, b.R, c);
            byte cG = (byte)Lerp(a.G, b.G, c);
            byte cB = (byte)Lerp(a.B, b.B, c);
            byte cA = (byte)Lerp(a.A, b.A, c);
            return new Color(cR, cG, cB, cA);   
        }

        public static float RandomFloat(float min, float max)
        {
            return Lerp(min, max, (float)_random.NextDouble());
        }

        public static float RandomFloat()
        {
            return (float)_random.NextDouble();
        }

        public static float DegreeToRadian(float angle)
        {
            return angle * (180.0f / (float)Math.PI);
        }

        public static Vector2f RandomVector2F(float min, float max)
        {
            return new Vector2f(RandomFloat(min, max), RandomFloat(min, max));
        }

        public static Color RandomColor(byte alpha)
        {
            byte[] bytes = new byte[3];
            _random.NextBytes(bytes);
            return new Color(bytes[0], bytes[1], bytes[2], alpha);
        }
    }
}
