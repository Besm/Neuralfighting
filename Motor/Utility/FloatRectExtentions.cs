﻿using SFML.Graphics;
using SFML.System;

namespace Motor.Utilities
{
    public static class FloatRectExtentions
    {
        public static bool Contains(this FloatRect floatRect, Vector2f v2F)
        {
            return floatRect.Contains(v2F.X, v2F.Y);
        }
    }
}