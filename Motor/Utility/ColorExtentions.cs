﻿using SFML.Graphics;

namespace Motor.Utilities
{
    public static class ColorExtentions
    {
        public static Color Inverted(this Color c)
        {
            return new Color((byte)(255 - c.R), (byte)(255 - c.G), (byte)(255 - c.B), 255);
        }
    }
}
