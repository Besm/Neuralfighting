﻿using System;
using System.Collections.Generic;
using System.Text;
using Motor.Utilities;
using SFML.System;

namespace Motor.Physics
{
    static class CollisionHandler
    {
        public static void Handle(Entity a, Entity b)
        {
            if (a is PhysicsEntity pa)
            {
                if (b is PhysicsEntity pb)
                {
                    Handle(pa, pb);
                    return;
                }
                else
                if (b is StaticEntity sb)
                {
                    Handle(pa, sb);
                    return;
                }
            }
            else
             if (b is PhysicsEntity pb2)
            {
                if (a is StaticEntity sa)
                {
                    Handle(pb2, sa);
                    return;
                }
            }
        }

        public static void Handle(PhysicsEntity a, StaticEntity b)
        {
            int i = 0;
            Vector2f closestPoint = new Vector2f(0, 0);
            ICollidable cA = (ICollidable)a;
            ICollidable cB = (ICollidable)b;
            switch (cB.Collider)
            {
                case CircleCollider _:
                    closestPoint = b.LastPosistion;
                    break;
                case RectangleCollider rc:
                    if (cA.Collider is CircleCollider Acc)
                    {
                        closestPoint = rc.ClosestPoint(Acc);
                    }

                    break;
                default:
                    throw new Exception("Collider type was not expected");
            }
            Vector2f normalDir = (a.LastPosistion - closestPoint).Normalized();


            while (cA.Collider.Intersect(cB.Collider))
            {
                a.Position += normalDir / 50;
                i++;
                if (i >= 1000) { break; }
            }

            Vector2f collisionNormal = (a.LastPosistion - closestPoint).Normalized();
            a.AddForce(collisionNormal * a.Velocity.Magnitude() * 5);
            a.Velocity = Mathf.Lerp(a.Velocity, new Vector2f(0, 0), .5f);
        }


        public static void Handle(PhysicsEntity a, PhysicsEntity b)
        {
            ICollidable cA = (ICollidable)a;
            ICollidable cB = (ICollidable)b;

            int i = 0;
            Vector2f normalDir = (a.LastPosistion - b.LastPosistion).Normalized();

            while (cA.Collider.Intersect(cB.Collider))
            {
                a.Position += normalDir / 50;
                b.Position -= normalDir / 50;
                i++;
                if (i >= 1000) { break; }
            }

            Vector2f relativeVelocity = a.Velocity - b.Velocity;
            Vector2f collisionNormal = (a.LastPosistion - b.LastPosistion).Normalized();
            a.AddForce(collisionNormal * relativeVelocity.Magnitude() * 5);
            b.AddForce(-(collisionNormal * relativeVelocity.Magnitude() * 5));
            a.Velocity = Mathf.Lerp(a.Velocity, new Vector2f(0, 0), .5f);
            b.Velocity = Mathf.Lerp(b.Velocity, new Vector2f(0, 0), .5f);
        }
    }
}
