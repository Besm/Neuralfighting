﻿using System;
using SFML.System;
using SFML.Graphics;
using Motor.Physics;
using Motor.Utilities;

namespace Motor
{
    public class RectangleCollider : Collider
    {
        public Vector2f Size { get; set; }

        public FloatRect GetCollisionRect()
        {
            return new FloatRect(Parent.Position + Offset, Size);
        }

        public RectangleCollider(Entity parent) : base(parent)
        {
            Size = parent.Size;
        }

        public Vector2f ClosestPoint(CircleCollider other)
        {
            return Mathf.MinMax(OffsettedPosistion(), OffsettedPosistion() + Size, other.OffsettedPosistion());

        }

        public override bool Intersect(Collider other) 
        {
            switch (other)
            {
                case RectangleCollider otherRect:
                    return GetCollisionRect().Intersects(otherRect.GetCollisionRect());
                case CircleCollider circleCollider:
                    Vector2f closestPoint = ClosestPoint(circleCollider);
                    return Mathf.Distance(closestPoint, circleCollider.OffsettedPosistion()) < circleCollider.Radius;

            }
            throw new Exception("Unknown collider type encounterd");
        }
    }
}
