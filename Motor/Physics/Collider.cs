﻿using SFML.System;

namespace Motor.Physics
{
    public abstract class Collider
    {
        public Vector2f OffsettedPosistion()
        {
            return Parent.Position + Offset;
        }

        protected Collider(Entity parent)
        {
            this.Parent = parent;
        }

        public Vector2f Offset { get; set; }

        public Entity Parent { get; set; }


   

        public abstract bool Intersect(Collider other);

    }
}
