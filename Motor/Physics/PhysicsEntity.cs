﻿using Motor.Utilities;
using SFML.Graphics;
using SFML.System;
using System;
namespace Motor.Physics
{
    public abstract class PhysicsEntity : Entity, IFixedUpdate
    {
        private Vector2f _forceToAdd;
        private FloatRect _posBonds;


        protected PhysicsEntity(float xpos, float ypos) : base(xpos, ypos)
        {
            _posBonds = new FloatRect(new Vector2f(0, 0), (Vector2f)WindowController.WindowSize());
        }

        public Vector2f Velocity { get; set; }

        public float Mass { get; set; } = 15f;
        public float Friction { get; set; } = .1f;
        public FloatRect PosBonds { get => _posBonds; set => _posBonds = value; }

        public void AddForce(Vector2f force)
        {
            _forceToAdd += force;
        }


        public virtual void FixedUpdate()
        {
            Velocity += _forceToAdd / Mass;
            Velocity = Mathf.Lerp(Velocity, new Vector2f(0, 0), Friction);
            _forceToAdd = new Vector2f(0, 0);
            Position += Velocity * 30;


            if (_posBonds.Contains(Position.X, Position.Y) == false)
            {
                float newX = Mathf.MinMax(0, _posBonds.Width, Position.X);
                float newY = Mathf.MinMax(0, _posBonds.Height, Position.Y);
                Position = new Vector2f(newX, newY);
                Velocity = new Vector2f(0, 0);
            }

           
        }
    }
}
