﻿using Motor.Physics;
using Motor.Utilities;
using System;

namespace Motor
{

    public class CircleCollider : Collider
    {
        public CircleCollider(Entity parent, float radius) : base(parent)
        {
            this.Parent = parent;
            this.Radius = radius;
        }

        public float Radius { get; set; }

        public override bool Intersect(Collider other)
        {
            switch (other)
            {
                case null:
                    return false;
                case CircleCollider c:
                    if (Mathf.Distance(Parent.Position + Offset, c.Parent.Position + c.Offset) < c.Radius + Radius)
                    {
                        return true;
                    }
                    return false;
                case RectangleCollider rect:
                    return rect.Intersect(this);
            }


            throw new NotImplementedException();
        }
    }
}
