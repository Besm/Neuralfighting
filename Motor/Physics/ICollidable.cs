﻿using Motor.Physics;

namespace Motor
{
    public interface ICollidable
    {
        Collider Collider { get;}
        void OnCollisionEnter(Entity other);

    }
}
