﻿using SFML.System;
using System;

namespace Motor.UI
{
    public class InputBox : UiEntity
    {
        private int _cursorIndex;

        public InputBox(float xpos, float ypos) : base(xpos, ypos)
        {
            LineText = new LineText(xpos, ypos);
        }

        public string Text { get; private set; } = "";
        public LineText LineText { get; set; }

        public Vector2f CursorPos()
        {
            Vector2f pos = LineText.TextShape.FindCharacterPos((uint)_cursorIndex);
            return pos;
        }

        public void Clear()
        {
            Text = "";
            _cursorIndex = 0;
        }

        public override void Draw()
        {

            LineText.Draw();

        }

        public override void PreDraw()
        {
            if (Input.GetKeyPressed(SFML.Window.Keyboard.Key.Left))
            {
                if (_cursorIndex > 0)
                {
                    _cursorIndex--;
                }
            }

            if (Input.GetKeyPressed(SFML.Window.Keyboard.Key.Right))
            {
                if (_cursorIndex < Text.Length)
                {
                    _cursorIndex++;
                }
            }

            string toAdd = Input.KeyInputStringLastFrame();
            for (int i = toAdd.Length - 1; i >= 0; i--)
            {
                if (toAdd[i] == '\b')
                {
                    if (Text.Length > 0 && _cursorIndex != 0)
                    {
                        Text = Text.Remove(_cursorIndex - 1, 1);
                    }
                    if (_cursorIndex > 0)
                    {
                        _cursorIndex--;
                    }
                    toAdd = toAdd.Trim(toAdd[i]);
                    break;
                }
 
                if (Char.IsControl(toAdd[i]) || toAdd[i] == '`')
                {
                    toAdd = toAdd.Trim(toAdd[i]);
                    break;
                }
            }
            Text = Text.Insert(_cursorIndex, toAdd);
            _cursorIndex += toAdd.Length;

            LineText.Position = Position;
            LineText.SetDisplayString(Text);
        }
    }
}
