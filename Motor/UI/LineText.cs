﻿using SFML.Graphics;

namespace Motor.UI
{
    public class LineText : UiEntity
    {
        public uint CharacterSize { get => TextShape.CharacterSize; set => TextShape.CharacterSize = value; }
        public string DisplayedString { get => TextShape.DisplayedString; }
        public Text TextShape { get; }
        public float MaxXpos { get; set; } = 1000;

        public void SetDisplayString(string s)
        {
            TextShape.DisplayedString = s;
            for (int i = 0; i < TextShape.DisplayedString.Length; i++)
            {
                if (TextShape.FindCharacterPos((uint)i).X > MaxXpos)
                {

                    TextShape.DisplayedString = TextShape.DisplayedString.Insert(i, "\n");
                }
            }
        }
        public LineText(float xpos, float ypos) : base(xpos, ypos)
        {
            Font font = ResourceManager.Get<Font>("TerminusTTF-4.46.0.ttf");
            TextShape = new Text("", font);
            TextShape.Color = Color.White;
            TextShape.Style = Text.Styles.Regular;

        }

        public override void Draw()
        {
            TextShape.Position = Position;
            TextShape.Scale = Size;
            Drawer.Draw(TextShape);
        }

        public override void PreDraw()
        {

        }
    }
}
