﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Motor.System;
using Motor.Utilities;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace Motor.UI
{
    public class Terminal : UiEntity
    {
        public static Terminal Current;
        private readonly List<MethodInfo> _commandMethods = new List<MethodInfo>();
        private readonly uint _lineHeight = 32;
        private RenderTexture _renderTexture;
        private Sprite _sprite;
        private bool _autoScroll = true;
        private float _backShade;
        private bool _extended;
        private Message _highlightedMessage;
        private float _scrollOffset;
        private FloatRect messagesRect;
        private bool isMoving;
        

        public List<Message> MessageHistory { get; set; } = new List<Message>();
        private LineText Line { get; }
        internal InputBox Inputbox { get; set; }
        public Color BackColor { get; set; }
        public Color FrontColor { get; set; }

        public Terminal(float x, float y) : base(x, y)
        {
            Current = this;
            Size = new Vector2f(WindowController.GetWidht(), 400);
            _renderTexture = new RenderTexture((uint)Size.X, (uint)Size.Y);
            _sprite = new Sprite(_renderTexture.Texture);
            Line = new LineText(0, 0) { CharacterSize = _lineHeight };
            Inputbox = new InputBox(x, y) { LineText = { CharacterSize = _lineHeight } };
            FrontColor = Color.White;
            BackColor = Color.Black;
            messagesRect = new FloatRect(new Vector2f(Size.X / 2, 0) + Position, new Vector2f(Size.X / 2, Size.Y));
            FindAndAddCommandMethods(Assembly.GetCallingAssembly());
            FindAndAddCommandMethods(Assembly.GetExecutingAssembly());
            
        }


        private void FindAndAddCommandMethods(Assembly assembly)
        {
            Type[] classesInAsssembly = assembly.GetTypes();

            foreach (Type type in classesInAsssembly)
            {
                foreach (MethodInfo methodInfo in type.GetMethods())
                {
                    if (methodInfo.GetCustomAttributes(typeof(TerminalCommandAttribute), true).Length > 0)
                    {
                        _commandMethods.Add(methodInfo);
                    }
                }
            }
        }

        private void UpdateHighlightedMessage()
        {
            if (Input.GetButtonPressed(Mouse.Button.Left) && messagesRect.Contains(Input.RelativeMousePosistion - Position))
            {
                float y = Input.RelativeMousePosistion.Y - Position.Y + _scrollOffset;
                int i = (int)(y / _lineHeight);
                if (i >= 0 && i < MessageHistory.Count) _highlightedMessage = MessageHistory[i];
            }
        }

        private void Scroll()
        {
           
            if (messagesRect.Contains(Input.RelativeMousePosistion) && !isMoving)
            {
                if (Input.GetMouseWheelYSmoothed() > 0) _autoScroll = false;

                _scrollOffset -= Input.GetMouseWheelYSmoothed() * 3.0f;
            }
            if (_autoScroll)
            {

                _scrollOffset = Mathf.Lerp(_scrollOffset, MaxScrollOffset(), .15f);
            }
            else
            {
                if (_scrollOffset > MaxScrollOffset() - _lineHeight * 0.5) _autoScroll = true;
            }

            _scrollOffset = Mathf.MinMax(0, MaxScrollOffset(), _scrollOffset);
        }

        private float MaxScrollOffset()
        {
            return Math.Max(0, (MessageHistory.Count - 12) * _lineHeight);
        }

        public static void AddMessage(Message message)
        {
            Current?.MessageHistory.Add(message);
        }

        private void ReDrawRenderTexture()
        {
            Drawer.SetRenderTarget(_renderTexture);
            _renderTexture.Clear(BackColor);
            DrawMessageHistory();

            DrawForegroundLines();
            DrawHighlightedInfo();

            _sprite.Position = Position;
            _renderTexture.Display();
            Drawer.SetRenderTarget(WindowController.GetWindowTarget());
        }

        private void DrawMessageHistory()
        {
            int iMin = (int)(_scrollOffset / _lineHeight);
            int iMax = Math.Min(iMin + 1 + (int)(Size.Y / _lineHeight), MessageHistory.Count);
            for (int i = iMin; i < iMax; i++)
            {
                Line.Position = new Vector2f(Size.X / 2 + 2, _lineHeight * i - _scrollOffset);
                Line.SetDisplayString(MessageHistory[i].Title);
                Line.Draw();
            }
        }

        private void DrawHighlightedInfo()
        {
            Line.SetDisplayString(Inputbox.Text);
            Line.Position = new Vector2f(2, Size.Y - _lineHeight - 5);
            Line.Draw();
            if (_highlightedMessage == null) return;
            Line.Position = new Vector2f(2, 0);
            Line.MaxXpos = WindowController.GetWidht() / 2 - 10;
            Line.SetDisplayString(_highlightedMessage.Title);
            Line.Draw();
            Line.Position = new Vector2f(2, 4 + _lineHeight);
            Line.SetDisplayString(_highlightedMessage.FullInfo);
            Line.Draw();
        }

        private void DrawForegroundLines()
        {
            Drawer.DrawPrimitiveLine(new Vector2f(Inputbox.CursorPos().X, Size.Y - _lineHeight),
                new Vector2f(Inputbox.CursorPos().X, Size.Y), FrontColor);
            Drawer.DrawPrimitiveLine(new Vector2f(0, 0), new Vector2f(Size.X, 0), FrontColor);
            Drawer.DrawPrimitiveLine(new Vector2f(0, Size.Y - _lineHeight),
                new Vector2f(Size.X / 2, Size.Y - _lineHeight), FrontColor);
            Drawer.DrawPrimitiveLine(new Vector2f(0, _lineHeight + 6), new Vector2f(Size.X / 2, _lineHeight + 6),
                FrontColor);
            Drawer.DrawPrimitiveLine(new Vector2f(1, 0), new Vector2f(1, Size.Y), FrontColor);
            Drawer.DrawPrimitiveLine(new Vector2f(Size.X / 2, 0), new Vector2f(Size.X / 2, Size.Y), FrontColor);
            Drawer.DrawPrimitiveLine(new Vector2f(0, Size.Y - 1), new Vector2f(Size.X, Size.Y - 1), FrontColor);
            Drawer.DrawPrimitiveLine(new Vector2f(Size.X - 1, Size.Y), new Vector2f(Size.X - 1, 0), FrontColor);

        }

        public override void Draw()
        {
            if (!_extended) return;
            ReDrawRenderTexture();

            Drawer.DrawRect(new Vector2f(0, 0), (Vector2f)WindowController.WindowSize(),
                new Color(0, 0, 0, (byte)(_backShade * 150)));
            Drawer.Draw(_sprite);
        }


        public void RunCommand(string fullCommand)
        {
            bool error = false;
            object result = null;
            List<string> splitted = fullCommand.Split(" ".ToCharArray()).ToList();
            MethodInfo methodInfo = _commandMethods.Find(m => m.Name == splitted[0]);
            if (methodInfo == null)
            {
                result = "Could not find method " + splitted[0];
                error = true;
            }

            splitted.RemoveAt(0);
            List<object> parameters = new List<object>();
            int i = 0;
            if (!error)
                foreach (string par in splitted)
                {
                    try
                    {
                        parameters.Add(Convert.ChangeType(par, methodInfo.GetParameters()[i].ParameterType));
                    }
                    catch (InvalidCastException)
                    {
                        result = "Could not cast " + par + " from string to " +
                                 methodInfo.GetParameters()[i].ParameterType;
                        error = true;
                        break;
                    }
                    catch (FormatException)
                    {
                        result = par + " is not in a correct format";
                        error = true;
                        break;
                    }
                    catch (OverflowException)
                    {
                        result = "Overflow error";
                        error = true;
                        break;
                    }

                    i++;
                }

            if (!error)
                try
                {
                    result = methodInfo.Invoke(this, parameters.ToArray());
                }
                catch (TargetParameterCountException)
                {
                    result = "Parameter count is not correct";
                    error = true;
                }
                catch (TargetException)
                {
                    result = "Can not call non static methods";
                    error = true;
                }

            if (result == null) result = "";
            MessageType messageType = MessageType.Message;
            if (error) messageType = MessageType.Error;
            AddMessage(new Message(fullCommand, result.ToString() ?? "No result", messageType));
            _highlightedMessage = MessageHistory.Last();
        }


        [TerminalCommand]
        public static void Move()
        {
            Current.isMoving = true;
            Mouse.SetPosition((Vector2i)(Input.RelativeMousePosistion + Current.Position));
        }

        [TerminalCommand]
        public static string ResetTerminal()
        {
            Current.Size = new Vector2f(WindowController.GetWidht(),400);
            Current.Reset();
            return "Terminal resetted";
        }
        [TerminalCommand]
        public static string Echo(string s)
        {
            return s;
        } 

        void Reset()
        {
            _renderTexture.Dispose();
            _sprite.Dispose();
            _renderTexture = new RenderTexture((uint)Current.Size.X, (uint)Current.Size.Y);
            _sprite = new Sprite(Current._renderTexture.Texture);
             messagesRect = new FloatRect(new Vector2f(Current.Size.X / 2, 0) + Current.Position, new Vector2f(Current.Size.X / 2, Current.Size.Y));
        }

        public override void PreDraw()
        {
            if (Input.GetKeyPressed(SFML.Window.Keyboard.Key.Left))
            {
                SceneManager.PreviousScene();
            }

            if (Input.GetKeyPressed(SFML.Window.Keyboard.Key.Right))
            {
                SceneManager.NextScene();
            }

            if (isMoving)
            {
                if (Input.GetButtonPressed(Mouse.Button.Left))
                {
                    isMoving = false;

                }

                if (Input.GetKey(Keyboard.Key.LShift))
                {
                    Position = new Vector2f(0, Input.RelativeMousePosistion.Y);
                }
                else Position = Input.RelativeMousePosistion;

                float c = Input.GetMouseWheelYSmoothed() *10f;
                Size -= Size.Normalized() * c;
                Size = Mathf.MinMax(Size.Normalized() * 1000, Size.Normalized() * 20000, Size);


                Reset();

            }

            Scroll();
            UpdateHighlightedMessage();
            if (Input.GetKeyPressed(Keyboard.Key.Return))
            {
                RunCommand(Inputbox.Text);
                Inputbox.Clear();
            }

            if (Input.GetKeyPressed(Keyboard.Key.Tilde))
            {
                SceneManager.Active.SetPaused(!_extended);
                _extended = !_extended;
            }

            if (_extended)
            {
                _backShade = Mathf.Lerp(_backShade, 1, .025f);
                Inputbox.PreDraw();
            }
            else
            {
                _backShade = Mathf.Lerp(_backShade, 0, .1f);
            }
        }
    }
}