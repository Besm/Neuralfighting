﻿namespace Motor
{

    public abstract class UiEntity : Entity, IDrawable
    {

        protected UiEntity(float xpos, float ypos) : base(xpos, ypos)
        {
        }


        public abstract void Draw();


        public abstract void PreDraw();
    }
}
