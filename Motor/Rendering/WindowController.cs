﻿using System;
using Motor.System;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace Motor
{
    public static class WindowController
    {
        public static RenderWindow RenderingWindow { get; private set; }
        public static uint TargetFps { get; set; }
        public static Clock RefreshClock { get; set; }

        public static uint GetWidht()
        {
            return WindowSize().X;
        }

        public static void SetSize(Vector2u size)
        {
            RenderingWindow.Size = size;
            RenderingWindow.SetView(new View(new FloatRect(new Vector2f(0, 0), (Vector2f)size)));
        }

        public static void CenterWindow()
        {
            uint x = (VideoMode.DesktopMode.Width - RenderingWindow.Size.X) / 2;
            uint y = (VideoMode.DesktopMode.Height - RenderingWindow.Size.Y) / 2;
            SetPosistion(new Vector2i((int)x, (int)y));
        }

        public static void SetPosistion(Vector2i pos)
        {
            RenderingWindow.Position = pos;
        }

        public static void SetTitle(string title)
        {
            RenderingWindow.SetTitle(title);
        }

        public static uint GetHeight()
        {
            return WindowSize().Y;
        }


        public static bool IsOpen()
        {
            return RenderingWindow.IsOpen;
        }

        public static RenderTarget GetWindowTarget()
        {
            return RenderingWindow;
        }

        public static Vector2u WindowSize()
        {
            return RenderingWindow.Size;
        }

        public static float TimeSinceLastDrawCall()
        {
            return RefreshClock.ElapsedTime.AsSeconds();
        }


        [TerminalCommand]
        public static string SetFixedMultiplier(int t)
        {
            FixedTime.Multiplier = t;
            return "Multiplier set to " + t;
        }

        public static Vector2f WindowRelativeMousePosistion()
        {
            return (Vector2f)Mouse.GetPosition(RenderingWindow);
        }

        public static void PreDraw()
        {
            RefreshClock.Restart();
            RenderingWindow.DispatchEvents();
            RenderingWindow.Clear(new Color(15, 15, 15, 255));
        }

        public static void AfterDraw()
        {
            RenderingWindow.Display();
        }



        public static void Initilize()
        {
            TargetFps = 60;
            VideoMode vm = new VideoMode(500, 500);
            RenderingWindow = new RenderWindow(vm, "Motor");
            CenterWindow();
            RenderingWindow.Closed += WindowClosed;
            //   Vector2i center = new Vector2i((int)SystemParameters.VirtualScreenWidth / 2, (int)SystemParameters.VirtualScreenHeight / 2);
            //renderWindow.Position = center - (Size / 2);
            //  renderWindow.SetFramerateLimit(targetFPS);
            RefreshClock = new Clock();
            RenderingWindow.MouseWheelMoved += Input.MouseWheelMovedEvent;
            RenderingWindow.TextEntered += Input.TextEnteredEvent;
        }

        private static void WindowClosed(object sender, EventArgs e)
        {
            RenderingWindow.Close();
        }
    }
}