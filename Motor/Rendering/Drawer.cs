﻿using SFML.Graphics;
using SFML.System;
namespace Motor
{
    public static class Drawer
    {
        private static RenderTarget _rendertarget;

        public static void SetRenderTarget(RenderTarget renderTarget)
        {

            _rendertarget = renderTarget;
        }


        public static void Draw(Text text)
        {

            _rendertarget.Draw(text);
        }

        public static void Draw(Sprite sprite)
        {
            _rendertarget.Draw(sprite);
        }

        public static void Draw(CircleShape circleShape)
        {
            _rendertarget.Draw(circleShape);
        }

        public static void Draw(VertexArray vertexArray)
        {
            _rendertarget.Draw(vertexArray);
        }

        public static void DrawPrimitiveLine(Vector2f from, Vector2f to, Color c)
        {
            VertexArray va = new VertexArray(PrimitiveType.Lines, 2)
            {
                [0] = new Vertex(from, c),
                [1] = new Vertex(to, c)
            };
            _rendertarget.Draw(va);
        }


        public static void DrawRect(Vector2f pos, Vector2f size, Color c)
        {
            RectangleShape rs = new RectangleShape(size)
            {
                FillColor = c,
                Position = pos
            };
            _rendertarget.Draw(rs);
        }


        public static void DrawCenterdRect(Vector2f pos, Vector2f size, Color c)
        {
            RectangleShape rs = new RectangleShape(size)
            {
                FillColor = c,
                Position = pos - size / 2,
                Size = size
                
            };
            _rendertarget.Draw(rs);
        }

        public static void Draw(RectangleShape rectangleShape)
        {
            _rendertarget.Draw(rectangleShape);
        }
    }
}
