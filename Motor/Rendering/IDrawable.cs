﻿namespace Motor
{
    public interface IDrawable
    {
        void PreDraw();
        void Draw();
    }
}
