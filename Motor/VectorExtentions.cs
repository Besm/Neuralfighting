﻿using System;
using SFML.System;

namespace Motor
{
    public static class VectorExtentions
    {
        public static Vector2f Normalized(this Vector2f v2F)
        {
            float length = Mathf.Length(v2F);

            return v2F / length;
        }


        public static float ToDegrees(this Vector2f v2F)
        {
            return Mathf.DegreeToRadian((float)Math.Atan2(v2F.Y, v2F.X));
        }

        public static float Magnitude(this Vector2f v2F)
        {
            return Mathf.Sqrt(v2F.X * v2F.X + v2F.Y * v2F.Y);
        }

    }
}
