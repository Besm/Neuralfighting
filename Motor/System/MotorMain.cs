﻿using SFML.System;
using System;

namespace Motor
{
    public abstract class MotorMain
    {
        private readonly Clock _updateTime;
        private int _updatesLastSecond;
        private int _updatesLastDrawCall;
        private readonly Clock _upsClock = new Clock();

        public static MotorMain Current { get; set; }
        SceneManager sceneManager = new SceneManager();


        public void WorldStep()
        {
            Input.RelativeMousePosistion = WindowController.WindowRelativeMousePosistion();

            FixedTime.UpdatesSinceStart++;

            SceneManager.Active?.FixedUpdate();
            SceneManager.Overlayed.FixedUpdate();


        }


        public void UpdateFrame()
        {
            _updateTime.Restart();

            UpdateCorrectAmount();

      /*      if (_upsClock.ElapsedTime.AsSeconds() >= 1.0f)
            {
                Terminal.AddMessage(new Message(("UPS = " + _updatesLastSecond / _upsClock.ElapsedTime.AsSeconds()),
                    "Updates per seconds =  \n" + _updatesLastSecond / _upsClock.ElapsedTime.AsSeconds()));
                _upsClock.Restart();
                _updatesLastSecond = 0;
            }*/
            if (WindowController.TimeSinceLastDrawCall() >= 1.0 / WindowController.TargetFps)
            {
                Input.RefreshInput();
                WindowController.PreDraw();
                SceneManager.Active?.PreDraw();
                SceneManager.Overlayed.PreDraw();
                SceneManager.Active?.Draw();
                SceneManager.Overlayed.Draw();
                _updatesLastDrawCall = 0;
                WindowController.AfterDraw();
            }
        }

        private void UpdateCorrectAmount()
        {
            switch (FixedTime.UpdateSpeed)
            {
                case UpdateSpeed.TargetFps:

                    if (_updatesLastDrawCall < 1)
                    {
                        WorldStep();
                        _updatesLastSecond++;
                        _updatesLastDrawCall++;
                    }
                    break;

                case UpdateSpeed.Multiplyer:
                    for (int i = 0; i < FixedTime.Multiplier - _updatesLastDrawCall; i++)
                    {
                        WorldStep();
                        _updatesLastDrawCall++;
                        _updatesLastSecond++;
                    }
                    break;

                case UpdateSpeed.TurboSpeed:
                    while (WindowController.TimeSinceLastDrawCall() < 1.0f / WindowController.TargetFps)
                    {
                        WorldStep();
                        _updatesLastSecond++;
                    }
                    break;
            }
        }

        protected MotorMain()
        {
            Current = this;
            WindowController.Initilize();
            Drawer.SetRenderTarget(WindowController.GetWindowTarget());
            _updateTime = new Clock();

        }
    }
}
