﻿using Motor.Physics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Motor
{
    public class Scene : IDrawable, IFixedUpdate
    {
        private List<Entity> Entities { get; } = new List<Entity>();
        private List<Entity> ToAdd { get; } = new List<Entity>();
        private List<Entity> GoingToDie { get; } = new List<Entity>();

        private bool _paused;
        private bool _isDrawn = true;


        public Scene()
        {
        }

        public void Load()
        {

        }


        public bool GetIsDrawn()
        {
            return _isDrawn;
        }

        public int Count()
        {
            return Entities.Count;
        }

        public void Remove(Entity e)
        {
            //   Entities.Remove(e);
            ToAdd.Remove(e);
        }

        public void SetIsDrawn(bool value)
        {
            _isDrawn = value;
        }

        public bool GetPaused()
        {
            return _paused;
        }

        public virtual void Clear()
        {
            Entities.Clear();
        }

        public void SetPaused(bool value)
        {
            _paused = value;
        }



        public void Add(Entity entity)
        {
            ToAdd.Add(entity);
        }

        public virtual void Draw()
        {
            if (!GetIsDrawn()) return;
            foreach (Entity entity in GetAllChildEntities(Entities))
            {
                if (entity is IDrawable drawable)
                {
                    drawable.Draw();
                }
            }
        }

        public virtual void PreDraw()
        {
            if (!GetIsDrawn()) return;
            foreach (Entity entity in GetAllChildEntities(Entities))
            {
                if (entity is IDrawable drawable)
                {
                    drawable.PreDraw();
                }
            }
        }



        protected void AddToAdds()
        {
            foreach (Entity entity in ToAdd)
            {
                Entities.Add(entity);
            }

            foreach (Entity child in GetAllChildEntities(ToAdd))
            {
                if (child is IStart startable)
                {
                    startable.Start();
                }
            }
            ToAdd.Clear();
        }

        private void CollisionChecker()
        {
            for (int i = 0; i < Entities.Count - 1; i++)
            {
                if (Entities[i] is ICollidable c1)
                {
                    for (int j = i + 1; j < Entities.Count; j++)
                    {
                        if (Entities[j] is ICollidable c2)
                        {

                            if (c1.Collider == null || c2.Collider == null || !c1.Collider.Intersect(c2.Collider)) continue;
                            CollisionHandler.Handle(Entities[i], Entities[j]);
                            c1.OnCollisionEnter(Entities[j]);
                            c2.OnCollisionEnter(Entities[i]);
                        }
                    }
                }
            }
        }

        private void RemoveDeathEntities()
        {
            foreach (Entity entity in GetAllChildEntities(Entities))
            {
                if (entity.WantsToDie)
                {
                    GoingToDie.Add(entity);
                }
            }

            foreach (Entity entity in GoingToDie)
            {
                Entities.Remove(entity);
            }

            GoingToDie.Clear();
        }

        List<Entity> GetAllChildEntities(List<Entity> entities)
        {
            List<Entity> all = new List<Entity>();
            all.AddRange(entities);
            foreach (Entity child in entities)
            {
                all.AddRange(child.GetAllChildren());
            }
            return all;
        }


        public virtual void FixedUpdate()
        {
            if (GetPaused()) return;
            CollisionChecker();
            foreach (Entity entity in GetAllChildEntities(Entities))
            {
                if (entity is IFixedUpdate updatable)
                {
                    updatable.FixedUpdate();
                }

            }

            AddToAdds();
            RemoveDeathEntities();
        }

    }
}
