﻿using System.Collections.Generic;

namespace Motor
{
    public class SceneManager
    {
        static List<Scene> _scenes = new List<Scene>();
        static Scene _active;
        static Scene _overlayed = new Scene();
        static int _curSceneNumber = 0;

        public static Scene Active { get => _active; }
        public static Scene Overlayed { get => _overlayed; set => _overlayed = value; }

        public static int GetCurSceneNumber()
        {
            return _curSceneNumber;
        }

        public static void SetCurSceneNumber(int value)
        {
            _curSceneNumber = value;

            GoToScene(_curSceneNumber);
        }

        public static void NextScene()
        {
            if (GetCurSceneNumber() < _scenes.Count - 1)
            {
                SetCurSceneNumber(GetCurSceneNumber() + 1);
            }
        }

        public static void PreviousScene()
        {
            if (GetCurSceneNumber() > 0)
            {
                SetCurSceneNumber(GetCurSceneNumber() - 1);
            }
        }


        public static void Add(Scene scene)
        {
            _scenes.Add(scene);
        }

        public static void GoToScene(int i)
        {
            if (_active != null)
            {
            }
            _active = _scenes[i];
        
        }
    }
}
