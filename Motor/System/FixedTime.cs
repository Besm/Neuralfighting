﻿using Motor.System;
using SFML.System;
using System;

namespace Motor
{
    public static class FixedTime
    {
        public static Clock GameClock { get; set; }
        public static float SecondsSinceStart => GameClock.ElapsedTime.AsSeconds();
        public static long UpdatesSinceStart { get; set; }
        public static int Multiplier { get; set; }
        public static UpdateSpeed UpdateSpeed { get; set; } = UpdateSpeed.TargetFps;

        [TerminalCommand]
        public static void FixedTimeMultiplier(int i)
        {
            Multiplier = i;
        }

        [TerminalCommand]
        public static string SetUpdateSpeedEnum(string i)
        {
            try
            {
                UpdateSpeed = (UpdateSpeed)Enum.Parse(typeof(UpdateSpeed), i);
                return "Changed speed to " + UpdateSpeed;
            }
            catch (Exception)
            {
                return "Can not find Updatespeed with the name " + i;
            }
        }
    }
}
