﻿namespace Motor.System
{
    public class TerminalCommands
    {
        [TerminalCommand]
        public static int Add(int a, int b)
        {
            return a + b;
        }


        [TerminalCommand]
        public static void Exit()
        {
            WindowController.RenderingWindow.Close();
        }
    }
}
