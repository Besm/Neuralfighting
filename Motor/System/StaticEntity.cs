﻿namespace Motor
{
    public abstract class StaticEntity : Entity
    {
        protected StaticEntity(float xpos, float ypos) : base(xpos, ypos)
        {
        }
    }
}
