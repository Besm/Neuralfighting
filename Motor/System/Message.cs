﻿namespace Motor
{
    public class Message
    {
        public MessageType Type { get; }

        public Message(string title, string fullInfo, MessageType messageType = MessageType.Message)
        {
            Title = title;
            FullInfo = fullInfo;           
            Type = messageType;
        }

        public string FullInfo { get; set; }
        public string Title { get; set; }
    }
}
