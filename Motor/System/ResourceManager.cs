﻿using System;
using System.Collections.Generic;

namespace Motor
{
    public static class ResourceManager
    {
        public static Dictionary<string, object> LoadedResources { get; } = new Dictionary<string, object>();

        private static void Load<T>(string fullpath, out T obj)
        {
                
            obj = (T)Activator.CreateInstance(typeof(T), fullpath);

            if (obj != null)
            {
                LoadedResources.Add(fullpath, obj);
            }
        }

        public static T Get<T>(string fullfilename)
        {
            LoadedResources.TryGetValue("Resources\\" + fullfilename, out object o);
            if (o != null)
            {
                return (T)o;
            }
            Load("Resources\\" + fullfilename, out T obj);
            return obj;
        }
    }
}
