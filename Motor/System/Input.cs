﻿using Motor.Utilities;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using static SFML.Window.Keyboard;
using static SFML.Window.Mouse;

namespace Motor
{
    public static class Input
    {
        private static HashSet<Key> _keyPressed = new HashSet<Key>();
        private static HashSet<Key> _keyGotPressedLastFrame = new HashSet<Key>();
        private static HashSet<Key> _keyGotReleasedLastFrame = new HashSet<Key>();

        private static HashSet<Button> _buttonPressed = new HashSet<Button>();
        private static HashSet<Button> _buttonGotPressedLastFrame = new HashSet<Button>();
        private static HashSet<Button> _buttonGotReleasedLastFrame = new HashSet<Button>();
        public static Vector2f RelativeMousePosistion;
        private static float _mouseWheelTicks;

        private static string _textEnteredLastFrame = "";

        internal static void RefreshInput()
        {
            _mouseWheelTicks = Mathf.Lerp(_mouseWheelTicks, 0.0f, .15f);
            _keyGotReleasedLastFrame.Clear();
            _keyGotPressedLastFrame.Clear();
            _buttonGotPressedLastFrame.Clear();
            _buttonGotReleasedLastFrame.Clear();
            _textEnteredLastFrame = "";

            foreach (Key val in Enum.GetValues(typeof(Key)))
            {
                if (GetKey(val) && !_keyPressed.Contains(val))
                {
                    _keyPressed.Add(val);
                    _keyGotPressedLastFrame.Add(val);
                }
                else if (!GetKey(val) && _keyPressed.Contains(val))
                {
                    _keyPressed.Remove(val);
                    _keyGotReleasedLastFrame.Add(val);
                }
            }

            foreach (Button val in Enum.GetValues(typeof(Button)))
            {
                if (GetButton(val) && !_buttonPressed.Contains(val))
                {
                    _buttonPressed.Add(val);
                    _buttonGotPressedLastFrame.Add(val);
                }
                else if (!GetButton(val) && _buttonPressed.Contains(val))
                {
                    _buttonPressed.Remove(val);
                    _buttonGotReleasedLastFrame.Add(val);
                }
            }
        }

        public static bool GetKey(Key key)
        {
            return IsKeyPressed(key);
        }

        public static bool GetKeyPressed(Key key)
        {
            return _keyGotPressedLastFrame.Contains(key);
        }

        public static bool GetKeyReleased(Key key)
        {
            return _keyGotReleasedLastFrame.Contains(key);
        }

        public static bool GetButton(Button b)
        {
            return IsButtonPressed(b);
        }

        public static bool GetButtonPressed(Button key)
        {
            return _buttonGotPressedLastFrame.Contains(key);
        }

        public static string KeyInputStringLastFrame()
        {
            return _textEnteredLastFrame;
        }

        internal static void TextEnteredEvent(object sender, TextEventArgs e)
        {
           // if (e.Unicode != "\b")
          //  {
                _textEnteredLastFrame += e.Unicode;
          //  }
        }
        internal static void MouseWheelMovedEvent(object sender, MouseWheelEventArgs e)
        {
            _mouseWheelTicks += e.Delta;
        }

        public static float GetMouseWheelYSmoothed()
        {
            return _mouseWheelTicks;
        }


        public static bool GetButtonReleased(Button key)
        {
            return _buttonGotReleasedLastFrame.Contains(key);
        }
    }
}
