﻿using SFML.System;
using System.Collections.Generic;

namespace Motor
{

    public abstract class Entity
    {
        protected Entity(float xpos, float ypos)
        {
            _position = new Vector2f(xpos, ypos);
            LastPosistion = Position;
        }


        public void AddChild(Entity entity)
        {
            children.Add(entity);
            entity.parent = this;
        }

        public void RemoveChild(Entity entity)
        {
            children.Remove(entity);
            entity.parent = null;

        }

        public IReadOnlyList<Entity> GetDirectChildren()
        {
            return children;
        }

        public IReadOnlyList<Entity> GetAllChildren()
        {
            List<Entity> allChilds = new List<Entity>();
            allChilds.AddRange(GetDirectChildren());
            foreach (Entity entity in GetDirectChildren())
            {
                allChilds.AddRange(entity.GetAllChildren());
            }
            return allChilds;
        }

        private List<Entity> children = new List<Entity>();
        private Vector2f _position;
        private Entity parent;
        private Entity GetParent() { return parent; }
        public void Die() => WantsToDie = true;
        public float X => Position.X;
        public float Y => Position.Y;
        public Vector2f Position { get => _position; set { LastPosistion = _position; _position = value; } }
        public Vector2f Size { get; set; } = new Vector2f(1, 1);
        public Vector2f Rotation { get; set; }
        public bool WantsToDie { get; private set; }
        public Vector2f LastPosistion { get; private set; }



    }
}
