﻿using System;
using System.Collections.Generic;

namespace NeuralNet
{
    enum NodeType
    {
        Sensor,
        Hidden,
        Output
    }

    public struct ConnectionGene
    {
        int InnovationNumber;
        int fromGene;
        int toGene;
        float weight;
        bool enabled;
    }

    public struct Genome
    {
        List<ConnectionGene> genes;
    }

    public struct Node
    {
        float weight;
        float value;
    }


    public class NeuralNetwork
    {
        List<Node> nodes;
        Genome genome;


        public NeuralNetwork()
        {
            
        }

        static void Create(Genome genome)
        {
           
        }
    }
}
