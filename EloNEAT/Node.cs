﻿namespace EloNEAT
{
    public struct Node
    {
        float _value;
        uint _historicalMarker;

        public Node(uint historicalMarker)
        {
            _value = 0;
            _historicalMarker = historicalMarker;
        }

        public float Value { get => _value; set => _value = value; }
        public uint HistoricalMarker { get => _historicalMarker; set => _historicalMarker = value; }
    }
}
