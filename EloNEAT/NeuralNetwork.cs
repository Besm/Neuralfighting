﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloNEAT
{

    public class NeuralNetwork
    {
        private Genome _genome;
        private List<Node> _nodes;
        private List<Connection> _connections;

        public NeuralNetwork()
        {

        }

        public void Set(NeuralNetwork nn)
        {
            _nodes = nn._nodes;
            _genome = nn._genome;
        }

        public NeuralNetwork(Genome genome)
        {
            Set(Create(genome));
        }

        public static NeuralNetwork Create(Genome genome)
        {
            NeuralNetwork nn = new NeuralNetwork
            {
                _nodes = new List<Node>(),
                _connections = new List<Connection>(),
                _genome = genome
            };

            AddNodes(genome, nn);
            AddConnections(genome, nn);
            return nn;
        }

        private static void AddConnections(Genome genome, NeuralNetwork nn)
        {
            foreach (Gene gene in genome.Genes)
            {
                if (gene.Enabled)
                {
                    nn._connections.Add(new Connection(gene));
                }
            }
        }

        private static void AddNodes(Genome genome, NeuralNetwork nn)
        {
            foreach (Gene gene in genome.Genes)
            {
                if (gene.Enabled)
                {
                    if (nn._nodes.Where(n => n.HistoricalMarker == gene.FromGene).Count() == 0)
                    {
                        nn._nodes.Add(new Node(gene.FromGene));
                    }
                    if (nn._nodes.Where(n => n.HistoricalMarker == gene.ToGene).Count() == 0)
                    {
                        nn._nodes.Add(new Node(gene.ToGene));
                    }
                }
            }
        }
    }
}
