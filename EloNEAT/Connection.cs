﻿namespace EloNEAT
{
    public class Connection
    {
        uint _fromGene;
        uint _toGene;
        float _weight;

        public Connection(uint fromGene, uint toGene, float weight)
        {
            FromGene = fromGene;
            ToGene = toGene;
            Weight = weight;
        }

        public Connection(Gene gene)
        {
            _fromGene = gene.FromGene;
            _toGene = gene.ToGene;
            _weight = gene.Weight;
        }

        public uint FromGene { get => _fromGene; set => _fromGene = value; }
        public uint ToGene { get => _toGene; set => _toGene = value; }
        public float Weight { get => _weight; set => _weight = value; }
    }
}
