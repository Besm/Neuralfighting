﻿namespace EloNEAT
{
    public struct Gene
    {
        int _innovationNumber;
        NodeType _nodeType;
        uint _fromGene;
        uint _toGene;
        float _weight;
        bool _enabled;

        public Gene(int innovationNumber, NodeType nodeType, uint fromGene, uint toGene, float weight, bool enabled)
        {
            _innovationNumber = innovationNumber;
            _nodeType = nodeType;
            _fromGene = fromGene;
            _toGene = toGene;
            _weight = weight;
            _enabled = enabled;
        }

        public int InnovationNumber { get => _innovationNumber; }
        public uint FromGene { get => _fromGene; }
        public uint ToGene { get => _toGene; }
        public float Weight { get => _weight; }
        public bool Enabled { get => _enabled; }
    }
}
